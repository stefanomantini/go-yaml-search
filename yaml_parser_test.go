package yaml_search

import (
	"strings"
	"testing"
)

func Test_getKeyBySelector(t *testing.T) {
	type args struct {
		selector    string
		searchSpace string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "simple select",
			args: args{
				selector: "spring.datasource.password",
				searchSpace: `
    spring:
      datasource:
        url: jdbc:sqlserver://someEnv.database.windows.net;databaseName=somethign
        username: someRandomUsername
        password: #{sqlPassword}#
`,
			},
			want: "#{sqlPassword}#",
		},
		{
			name: "edge case for flyway password",
			args: args{
				searchSpace: `
    spring:
      datasource:
        url: jdbc:sqlserver://password.database.windows.net;databaseName=db
        username: svc_prod_IngestFileValidationService
        password: #{password1}#
      kafka:
        bootstrap-servers: kafka-cluster:9092;kafka-cluster:9092
      flyway:
        user: svc_flywayadminuser
        password: #{password2}#
    azure:
      application-insights:
        instrumentation-key: 1ffbfc1d-b3fe-4e67-9da9-cf521d34f860
        quick-pulse:
          enabled: true
      storage:
        datalake:
          endpoint: https://something.dfs.core.windows.net/ 
          sas: #{password3}#
        blob:
          key: #{password4}#
          connection:
            string: DefaultEndpointsProtocol=https;AccountName=something;AccountKey=${azure.storage.blob.key};EndpointSuffix=core.windows.net
`,
				selector: "spring.flyway.password",
			},
			want: "#{password2}#",
		},
		{
			name: "edge case for azure password",
			args: args{
				selector: "azure.storage.blob.key",
				searchSpace: `
    spring:
      datasource:
        url: jdbc:sqlserver://something
        username: username1
        password: #{password1}#
      kafka:
        bootstrap-servers: kafka:9092
      flyway:
        user: username2
        password: #{password2}#
    redis:
      database: 0
      hostname: host.redis.cache.windows.net
      password: #{redisPassword}#
    app:
      schema:
        definitions: validations, transaction, settlement, merchant, invoiceCommon
      auth:
        event-store:
          enable: true
        error-threshold: 30
        scheduler:
          enable: true
          cronExpression: 0 0/5 * * * *
      nov:
        scheduler:
          enable: true
          cronExpression: 0 0/2 * * * *
      adls-landing: auth
    azure:
      application-insights:
        instrumentation-key: someLongKey
        quick-pulse:
          enabled: true
      storage:
        datalake:
          endpoint: https://someEndpoint.dfs.core.windows.net/ 
          sas: #{datalakeSas}#
        blob:
          key: #{blobKey}#
          connection:
            string: DefaultEndpointsProtocol=https;AccountName=something;AccountKey=${azure.storage.blob.key};EndpointSuffix=core.windows.net

`,
			},
			want: "#{blobKey}#",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := FindKeyBySelector(tt.args.selector, tt.args.searchSpace)
			if (err != nil) != tt.wantErr {
				t.Errorf("\nerror  :'%v'\nwantErr :'%v'\n", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("\ngot:\t'%v'\nwant:\t'%v'\n", got, tt.want)
			}
		})
	}
}

func Test_getGroup_nesting_OK(t *testing.T) {
	// top level
	gotTopLevel, err := getGroup("azure", exampleYaml)
	if err != nil {
		t.Fatal(err)
	}

	wantTopLevel := `
    azure:
      application-insights:
        instrumentation-key: someLongKey
        quick-pulse:
          enabled: true
      storage:
        datalake:
          endpoint: https://someEndpoint.dfs.core.windows.net/ 
          sas: #{password4}#
        blob:
          key: #{password5}#
          connection:
            string: DefaultEndpointsProtocol=https;AccountName=something;AccountKey=${azure.storage.blob.key};EndpointSuffix=core.windows.net`
	if strings.TrimSpace(gotTopLevel) != strings.TrimSpace(wantTopLevel) {
		t.Fatalf("\ngotTopLevel:\t'%v'\nwantTopLevel:\t'%v'\n", gotTopLevel, wantTopLevel)
	}

	wantFirstNest := `
	  storage:
        datalake:
          endpoint: https://someEndpoint.dfs.core.windows.net/ 
          sas: #{password4}#
        blob:
          key: #{password5}#
          connection:
            string: DefaultEndpointsProtocol=https;AccountName=something;AccountKey=${azure.storage.blob.key};EndpointSuffix=core.windows.net`
	gotFirstNest, err := getGroup("storage", wantTopLevel)
	if err != nil {
		t.Fatal(err)
	}
	if strings.TrimSpace(gotFirstNest) != strings.TrimSpace(wantFirstNest) {
		t.Fatalf("\ngotFirstNest:\t'%v'\nwantFirstNest:\t'%v'\n", gotFirstNest, wantFirstNest)
	}

	wantSecondNest := `
        blob:
          key: #{password5}#
          connection:
            string: DefaultEndpointsProtocol=https;AccountName=something;AccountKey=${azure.storage.blob.key};EndpointSuffix=core.windows.net`
	gotSecondNest, err := getGroup("blob", wantFirstNest)
	if err != nil {
		t.Fatal(err)
	}
	if strings.TrimSpace(gotSecondNest) != strings.TrimSpace(wantSecondNest) {
		t.Fatalf("\ngotSecondNest:\t'%v'\nwantSecondNest:\t'%v'\n", gotSecondNest, wantSecondNest)
	}

	wantThirdNest := `key: #{password5}#`
	gotThirdNest, err := getGroup("key", wantSecondNest)
	if err != nil {
		t.Fatal(err)
	}
	if strings.TrimSpace(gotThirdNest) != strings.TrimSpace(wantThirdNest) {
		t.Fatalf("\ngotThirdNest:\t'%v'\nwantThirdNest:\t'%v'\n", gotThirdNest, wantThirdNest)
	}
}
func Test_getGroup(t *testing.T) {
	type args struct {
		selectorSegment string
		searchSpace     string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "redis case",
			args: args{
				selectorSegment: "redis",
				searchSpace:     exampleYaml,
			},
			want: `    redis:
      database: 0
      hostname: host.redis.cache.windows.net
      password: #{password3}#`,
			wantErr: false,
		},
		{
			name: "spring case",
			args: args{
				selectorSegment: "spring",
				searchSpace:     exampleYaml,
			},
			want: `    spring:
      datasource:
        url: jdbc:sqlserver://something
        username: username1
        password: #{password1}#
      kafka:
        bootstrap-servers: kafka:9092
      flyway:
        user: username2
        password: #{password2}#`,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getGroup(tt.args.selectorSegment, tt.args.searchSpace)
			if (err != nil) != tt.wantErr {
				t.Errorf("\nerr:\t'%v'\nwantErr:\t'%v'\n", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("\ngot:\t'%v'\nwant:\t'%v'\n", got, tt.want)
			}
		})
	}
}

func Test_findIndentationForSegment(t *testing.T) {
	type args struct {
		selectorSegment string
		searchSpace     string
	}
	tests := []struct {
		name            string
		args            args
		wantIndentation int
		wantStartLine   int
		wantFinishLine  int
	}{
		{
			name: "simple spring",
			args: args{
				selectorSegment: "spring",
				searchSpace: `
    spring:
      datasource:
        url: jdbc:sqlserver://prodsqlfg1.database.windows.net;databaseName=ProdUksSQLDBfilevalidation
        username: svc_prod_IngestFileValidationService
        password: #{sqlsvcfilevalidationdmlpassword}#
      kafka:
        bootstrap-servers: wn0-produk.s453p2ae4q4e1e5jhlsgdjpvcg.cwx.internal.cloudapp.net:9092,wn1-produk.s453p2ae4q4e1e5jhlsgdjpvcg.cwx.internal.cloudapp.net:9092,wn2-produk.s453p2ae4q4e1e5jhlsgdjpvcg.cwx.internal.cloudapp.net:9092
      flyway:
        user: svc_flywayadminuser
        password: #{sqlsvcfilevalidationddlpassword}#
    redis:
      database: 0
      hostname: produkwredis0.redis.cache.windows.net
      password: #{redisPassword}#

`,
			},
			wantIndentation: 4,
			wantStartLine:   1,
			wantFinishLine:  10,
		},
		{
			name: "simple case redis",
			args: args{
				selectorSegment: "redis",
				searchSpace: `
    spring:
      datasource:
        url: jdbc:sqlserver://prodsqlfg1.database.windows.net;databaseName=ProdUksSQLDBfilevalidation
        username: svc_prod_IngestFileValidationService
        password: #{sqlsvcfilevalidationdmlpassword}#
      kafka:
        bootstrap-servers: wn0-produk.s453p2ae4q4e1e5jhlsgdjpvcg.cwx.internal.cloudapp.net:9092,wn1-produk.s453p2ae4q4e1e5jhlsgdjpvcg.cwx.internal.cloudapp.net:9092,wn2-produk.s453p2ae4q4e1e5jhlsgdjpvcg.cwx.internal.cloudapp.net:9092
      flyway:
        user: svc_flywayadminuser
        password: #{sqlsvcfilevalidationddlpassword}#
    redis:
      database: 0
      hostname: produkwredis0.redis.cache.windows.net
      password: #{password3}#

`,
			},
			wantIndentation: 4,
			wantStartLine:   11,
			wantFinishLine:  14,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotIndentation, gotStartLine, gotFinishLine := findIndentationForSegment(tt.args.selectorSegment, tt.args.searchSpace)
			if gotIndentation != tt.wantIndentation {
				t.Errorf("\ngotIndentation:\t\t%v\nwantIndentation:\t\t%v", gotIndentation, tt.wantIndentation)
			}
			if gotStartLine != tt.wantStartLine {
				t.Errorf("\ngotStartLine:\t\t%v\nwantStartLine:\t\t%v\n", gotStartLine, tt.wantStartLine)
			}
			if gotFinishLine != tt.wantFinishLine {
				t.Errorf("\ngotFinishLine:\t\t%v\nwantFinishLine:\t\t%v\n", gotFinishLine, tt.wantFinishLine)
			}
		})
	}
}

var exampleYaml = `
    spring:
      datasource:
        url: jdbc:sqlserver://something
        username: username1
        password: #{password1}#
      kafka:
        bootstrap-servers: kafka:9092
      flyway:
        user: username2
        password: #{password2}#
    redis:
      database: 0
      hostname: host.redis.cache.windows.net
      password: #{password3}#
    app:
      schema:
        definitions: validations, transaction, settlement, merchant, invoiceCommon
      auth:
        event-store:
          enable: true
        error-threshold: 30
        scheduler:
          enable: true
          cronExpression: 0 0/5 * * * *
      nov:
        scheduler:
          enable: true
          cronExpression: 0 0/2 * * * *
      adls-landing: auth
    azure:
      application-insights:
        instrumentation-key: someLongKey
        quick-pulse:
          enabled: true
      storage:
        datalake:
          endpoint: https://someEndpoint.dfs.core.windows.net/ 
          sas: #{password4}#
        blob:
          key: #{password5}#
          connection:
            string: DefaultEndpointsProtocol=https;AccountName=something;AccountKey=${azure.storage.blob.key};EndpointSuffix=core.windows.net
      
    cosmos:
      host: https://something.documents.azure.com:443
      key: #{password6}#
      database: someDb

`
