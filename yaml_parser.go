package yaml_search

import (
	"fmt"
	"regexp"
	"strings"
	"unicode/utf8"
)

// FindKeyBySelector
// selector 'some.yaml.selector'
// searchSpace 'some YAML string'
func FindKeyBySelector(selector, searchSpace string) (string, error) {
	var searched = searchSpace
	for _, segment := range strings.Split(selector, ".") {
		group, err := getGroup(segment, searched)
		if err != nil {
			return "", err
		}
		searched = group
	}
	re, err := regexp.Compile("(#{)(.+)(?:}#)")
	if err != nil {
		return "", err
	}
	searchedBytes := []byte(searched)
	if !re.Match(searchedBytes) {
		return "", fmt.Errorf("error retrieving key for selector %s, found: %s", selector, searched)
	}
	return string(re.Find(searchedBytes)), nil
}

// getGroup returns a smaller search space based on the next segment, used for recursion
func getGroup(selectorSegment, searchSpace string) (string, error) {
	_, startLine, finishLine := findIndentationForSegment(selectorSegment, searchSpace)

	fileLines := strings.Split(searchSpace, "\n")

	group := strings.Join(fileLines[startLine:finishLine+1], "\n")

	return group, nil
}

// returns the indentation and start/finish lines for a given segment
func findIndentationForSegment(selectorSegment, searchSpace string) (indentation, startLine, finishLine int) {
	fileLines := strings.Split(searchSpace, "\n")
	for i, l := range fileLines {
		key := strings.Split(l, ":")
		if strings.TrimSpace(key[0]) == selectorSegment {
			indentation = countLeadingTabs(l)
			startLine = i
		}
	}

	firstNode := true
	for i, line := range fileLines {
		if i >= startLine {
			if i != startLine {
				firstNode = false
			}
			currentLeadingTabs := countLeadingTabs(line)
			if currentLeadingTabs <= indentation && !firstNode {
				break
			} else {
				finishLine = i
			}
		}
	}
	//fmt.Printf("indentation: %d, start: %d, finish: %d\n", indentation, startLine, finishLine)
	return indentation, startLine, finishLine
}

// counts the leading tabs/spaces, provided it's consistent this will work effectively but can lead to some pain...
func countLeadingTabs(line string) int {
	i := 0
	for _, runeValue := range line {
		tab, _ := utf8.DecodeRune([]byte("\t"))
		space := ' '
		//TODO check this hasn't broken anything...
		if runeValue == tab || runeValue == space {
			i++
		} else {
			break
		}
	}
	return i
}
